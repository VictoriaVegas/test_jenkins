package test_jenkins;

public class SayHello {
	public static final String HELLO = "Hello ";
	public static final String EXCLAMATION = " !";

	public static String sayHello(String name) {

		return HELLO + name + EXCLAMATION;
	}
}
